%define tcpslice_dir tcpslice-1.8

Name:           tcpdump
Epoch:          14
Version:        4.99.5
Release:        1
Summary:        A network traffic monitoring tool
License:        BSD-2-Clause AND BSD-3-Clause AND BSD-4-Clause AND BSD-4-Clause-UC AND ISC AND NTP
URL:            http://www.tcpdump.org

Source0:        http://www.tcpdump.org/release/tcpdump-%{version}.tar.gz
Source1:        https://www.tcpdump.org/release/%{tcpslice_dir}.tar.gz
Source2:        http://www.tcpdump.org/release/tcpdump-%{version}.tar.gz.sig

Patch0:         backport-0002-Use-getnameinfo-instead-of-gethostbyaddr.patch
Patch1:         backport-0003-Drop-root-priviledges-before-opening-first-savefile-.patch
Patch2:         backport-0007-Introduce-nn-option.patch
Patch3:         backport-0009-Change-n-flag-to-nn-in-TESTonce.patch
Patch4:         tcpdump-Add-sw64-architecture.patch
Patch5:         backport-pgm-fix-the-way-we-step-through-the-packet.patch
Patch6:         backport-pgm-don-t-advance-bp-by-the-option-haeder-length-twi.patch


Requires(pre):  shadow-utils
BuildRequires:  automake openssl-devel libpcap-devel git-core gcc make

%description
Tcpdump is a command-line tool for monitoring network traffic.
Tcpdump can capture and display the packet headers on a particular
network interface or on all interfaces.  Tcpdump can display all of
the packet headers, or just the ones that match particular criteria.

Install tcpdump if you need a program to monitor network traffic.

%package help
Summary:        Help documents for tcpdump

%description    help
Man pages and other related help documents for tcpdump.

%prep
%autosetup -a 1 -S git

%build
export CFLAGS="$RPM_OPT_FLAGS $(getconf LFS_CFLAGS) -fno-strict-aliasing -DGUESS_TSO"

pushd %{tcpslice_dir}
# update config.{guess,sub}
automake -a -f 2> /dev/null || :
./autogen.sh
%configure
make %{?_smp_mflags}
popd

%configure --with-crypto --with-user=tcpdump --without-smi
make %{?_smp_mflags}

%install
mkdir -p ${RPM_BUILD_ROOT}%{_libdir}
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man8
mkdir -p ${RPM_BUILD_ROOT}%{_sbindir}

pushd %{tcpslice_dir}
install -m755 tcpslice ${RPM_BUILD_ROOT}%{_sbindir}
install -m644 tcpslice.1 ${RPM_BUILD_ROOT}%{_mandir}/man8/tcpslice.8
popd

install -m755 tcpdump ${RPM_BUILD_ROOT}%{_sbindir}
install -m644 tcpdump.1 ${RPM_BUILD_ROOT}%{_mandir}/man8/tcpdump.8

# fix section numbers
sed -i 's/\(\.TH[a-zA-Z ]*\)[1-9]\(.*\)/\18\2/' ${RPM_BUILD_ROOT}%{_mandir}/man8/*

%pre
user_id=$(id -u tcpdump 2> /dev/null)

if [ "_${user_id}" = "_" ]; then
    # check and create group tcpdump first
    if ! getent group tcpdump &> /dev/null; then
     /usr/sbin/groupadd -g 72 tcpdump 2> /dev/null
    fi
    # group tcpdump may created by other, id is not sure
    /usr/sbin/useradd -r -u 72 -g tcpdump -d /home/tcpdump -M -s /sbin/nologin tcpdump 2> /dev/null
fi

%postun
user_id=$(id -u tcpdump 2> /dev/null)
group_id=$(id -g tcpdump 2> /dev/null)

if [ "_${user_id}" = "_72" ]; then
  # del user tcpdump created by rpm first
  /usr/sbin/userdel tcpdump 2> /dev/null

  # del group tcpdump created by rpm and group may be deleted when delete user
  if getent group tcpdump 2> /dev/null && [ "_${group_id}" = "_72" ]; then
    /usr/sbin/groupdel tcpdump 2> /dev/null
  fi
fi

%check
make check

%files
%doc README.md CHANGES CREDITS
%license LICENSE
%{_sbindir}/tcpdump
%{_sbindir}/tcpslice

%files help
%{_mandir}/man8/tcpslice.8*
%{_mandir}/man8/tcpdump.8*

%changelog
* Thu Feb 20 2025 xinghe <xinghe2@h-partners.com> - 14:4.99.5-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update tcpdump to 4.99.5

* Wed Jul 17 2024 tangkuigang <tkgup@qq.com> - 14:4.99.4-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:resolve user information residue after RPM package uninstallation, add user checks

* Tue May 07 2024 xinghe <xinghe2@h-partners.com> - 14:4.99.4-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix a not defined macro error

* Sat Apr 20 2024 xinghe <xinghe2@h-partners.com> - 14:4.99.4-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:revert "fix CVE-2024-2397"

* Mon Apr 08 2024 xinghe <xinghe2@h-partners.com> - 14:4.99.4-2
- Type:CVE
- CVE:CVE-2024-2397
- SUG:NA
- DESC:fix CVE-2024-2397

* Tue Jul 25 2023 xinghe <xinghe2@h-partners.com> - 14:4.99.4-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update tcpdump to 4.99.4

* Sat Apr 08 2023 gaihuiying <eaglegai@163.com> - 14:4.99.3-2
- Type:CVE
- CVE:CVE-2023-1801
- SUG:NA
- DESC:fix CVE-2023-1801

* Tue Jan 31 2023 xinghe <xinghe2@h-partners.com> - 14:4.99.3-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update tcpdump to 4.99.3

* Thu Dec 15 2022 xinghe <xinghe2@h-partners.com> - 14:4.99.1-5
- Type:feature
- CVE:NA
- SUG:NA
- DESC:Add sw64 architecture

* Wed Aug 31 2022 gaihuiying <eaglegai@163.com> - 14:4.99.1-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix git warning when rpmbuild -ba compiling

* Wed Aug 17 2022 zhangqiang <zhangqiang@kylinos.cn> - 14:4.99.1-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:Set SA_RESTART on non-lethal signals (REQ_INFO, FLUSH_PCAP) to avoid corrupting binary pcap output

* Sat Jun 11 2022 gaihuiying <eaglegai@163.com> - 4.99.1-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:enable make check

* Mon Mar 21 2022 yanglu<yanglu72@h-partners.com> - 4.99.1-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update tcpdump to 4.99.1

* Fri Dec 18 2020 seuzw <930zhaowei@163.com> - 4.9.3-4
- Type:CVE
- ID:CVE-2020-8037
- SUG:NA
- DESC:fix CVE-2020-8037

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.9.3-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.9.3-2
- Package init
